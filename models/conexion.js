import mysql from 'mysql2'

let conexion;
function createConnection(){
    conexion = mysql.createConnection({
        host: "127.0.0.1",
        user: "root",
        password: "RuanetRevan1138",
        database: "sistemas"
    });

    conexion.connect((err) => {
        if (err) {
            console.log("Surgió un error (conexion.js)" + err);
        } else {
            console.log("Se abrió la conexión con éxito");
        }
    });
    return conexion;
}

export function getConnection(){
    if(!conexion){
        return createConnection();
    }
    return conexion;
}
