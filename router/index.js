import express from "express";
import json from 'body-parser';
import alumnosDb from "../models/alumnos.js";

export const router = express.Router();

export default router;

router.get('/', (req, res) => {
    res.render('index', { titulo: "MIS PRACTICAS", nombre: "Ruanet Ozuna" });
});

router.get('/tabla', (req, res) => {
    const params = {
        numero: req.query.numero
    };
    res.render('tabla', params);
});

router.post('/tabla', (req, res) => {
    const params = {
        numero: req.body.numero
    };
    res.render('tabla', params);
});

router.get('/cotizacion', (req, res) => {
    const valor = parseFloat(req.query.valor);
    const pinicial = parseFloat(req.query.pinicial);
    const plazos = parseInt(req.query.plazos);

    const pagoInicial = valor * (pinicial / 100);
    const valorFinal = valor - pagoInicial;
    const cuotaMensual = valorFinal / plazos;

    const params = {
        valor: valor,
        pinicial: pinicial,
        plazos: plazos,
        pagoInicial: pagoInicial,
        totalFin: valorFinal,
        totalPlazos: cuotaMensual
    };

    res.render('cotizacion', params);
});

router.post('/cotizacion', (req, res) => {
    const valor = parseFloat(req.body.valor);
    const pinicial = parseFloat(req.body.pinicial);
    const plazos = parseInt(req.body.plazos);

    const pagoInicial = valor * (pinicial / 100);
    const valorFinal = valor - pagoInicial;
    const cuotaMensual = valorFinal / plazos;

    const params = {
        valor: valor,
        pinicial: pinicial,
        plazos: plazos,
        pagoInicial: pagoInicial,
        totalFin: valorFinal,
        totalPlazos: cuotaMensual 
    };

    res.render('cotizacion', params);
});



let rows;
router.get('/alumnos', async(req, res) => {
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos', {reg:rows});
});

let params;
router.post('/alumnos', async(req, res) => {
    try {
        params = {
            matricula: req.body.matricula,
            nombre: req.body.nombre,
            domicilio: req.body.domicilio,
            sexo: req.body.sexo,
            especialidad: req.body.especialidad
        };
        await alumnosDb.insertar(params);
    } catch (error) {
        console.error(error);
        res.status(400).send(`Sucedio un error (app.js): ${error}`);
    }
    rows = await alumnosDb.mostrarTodos();
    res.render('alumnos', {reg:rows});
});

router.get("/pago", (req, res) => {
    const params = {
      titulo: "PreExamen02 [Ruanet Ozuna]",
      numero: req.query.numero,
      nombre: req.query.nombre,
      domicilio: req.query.domicilio,
      servicio: req.query.servicio,
      kwConsumidos: req.query.kwConsumidos,
      isPost: false,
    };
    res.render("pago", params);
  });
  
  router.post("/pago", (req, res) => {
    const precios = [1.08, 2.5, 3.0];
    const { numero, nombre, domicilio, servicio, kwConsumidos } = req.body;
    const precioKw = precios[servicio * 1];
    const tipoDeServicio = servicio == 0 ? 'Domestico' :  servicio == 1 ? 'Comercial' : 'Industrial';
    const subtotal = precioKw * kwConsumidos;
    
    let descuento = 0;
    if (kwConsumidos <= 1000) {
      descuento = 0.1;
    } else if (kwConsumidos > 1000 && kwConsumidos <= 10000) {
      descuento = 0.2;
    } else {
      descuento = 0.5;
    }
  
    const descuentoAplicado = subtotal * descuento;
    const subtotalConDescuento = subtotal - descuentoAplicado;
    const impuesto = 0.16 * subtotalConDescuento;
    const total = subtotalConDescuento + impuesto;
    
    const params = {
      titulo: "PreExamen02 [Ruanet Ozuna]",
      numero,
      nombre,
      domicilio,
      servicio: tipoDeServicio,
      kwConsumidos,
      precioKw,
      subtotal,
      descuento: descuentoAplicado,
      subtotalConDescuento,
      impuesto,
      total,
      isPost: true,
    };
    
    res.render("pago", params);
  });
  